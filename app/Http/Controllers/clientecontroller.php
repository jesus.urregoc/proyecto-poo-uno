<?php

namespace app\Http\controllers;

use Illuminate\Http\request;
use app\cliente;

class clientecontroller extends controller
{
    public function iniciocliente(Reques $request)
     {
        $cliente = cliente::all();
        return view(`cliente.inicio`)->with(`cliente`,$cliente);
     }
     
    public function clearcliente(request $request)
    {
        $cliente = cliente::all();
        return view(`cliente.clear`)->with(`cliente`,$cliente);

    }

    public function guardarcliente(Request $request){
        $this->validate($request,[
            `nombre` => `required`,
            `apellido` => `required`,
            `cedula` => `required`,
            `direccion` => `required`,
            `telefono` => `required`,
            `fecha de nacimiento` => `required`,
            `email` => `required`,
        ]);

        $cliente = new cliente;
        $cliente->nombre=$request->nombre;
        $cliente->apellido=$request->apellido;
        $cliente->cedula=$request->cedula;
        $cliente->direccion=$request->direccion;
        $cliente->telefono=$request->telefono;
        $cliente->fecha_nacimiento=$request->fecha_nacimiento;
        $cliente->email=$request->email;
        $cliente->save();
        return redirect()->route(`list.clientes`);
    }
}

