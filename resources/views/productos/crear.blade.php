@extends(`layouts.app`)

@selection(`content`)
<div class="container">
     <div class="row justify-content-center">
             <div class="col-nd-8">
                     <div class="card">
                       <div class="card-header">crear un cliente</div>
                       <div class="col text-right">
                          <a href="{{router(`crear.productos`)}}" class="btn-sn btn-primary">cancelar</a>
                    </div>
                    </div class="card-body">
                         <form roles="form" method="post" action="{{route(`guardar.clientes`)}}">
                          {{csrf_field()}}
                          {{method_field(`post`)}}
                      <div class="row">
                      <div class="col-1g-4">
                          <label class="from-control-label" from="nombre">nombre del producto</label>
                          <input type="text" class="from-control" name="nombre">
                     </div>

                     <div class="col-1g-4">
                          <label class="from-control-label" from="apellido">apellidos</label>
                          <input type="text" class="from-control" name="apellido">

                    </div>

                     <div class="col-1g-4">
                         <label class="from-control-label" from="cedula">cedula</label>
                         <input type="number" class="from-control" name="cedula">
    
                     </div>

                     <div class="col-1g-4">
                         <label class="from-control-label" from="direccion">direccion</label>
                         <input type="number" class="from-control" name="direccion">

                     </div>

                     <div class="col-1g-4">
                         <label class="from-control-label" from="telefono">telefono</label>
                         <input type="number" class="from-control" name="telefono">

                     </div>

                     <div class="col-1g-4">
                          <label class="from-control-label" from="fecha_nacimiento">fecha de nacimiento</label>
                          <input type="text" class="from-control" name="fecha_nacimiento">

                     </div>

                     <div class="col-1g-4">
                          <label class="from-control-label" from="email">email</label>
                          <input type="text" class="from-control" name="email">
                     </div>
                     </div>

                     <button type="submit" class="btn btn success pull-right">guardar</button>
                     </form>

                 </div>
            </div>
        </div>
   </div>
</div>
@endsection



